#include <iostream>
#include <string>
#include <cstring>

using namespace std;

void cstring(char cword[], int);
void String(string, int);

int main()
{
	char ctext[100];	//cstring
	string text;	//string
	int clength, length;

	cout << "Input word : ";
	cin >> ctext;
	text = ctext;


	clength = strlen(ctext);	

	length = text.size();	


	cstring(ctext, clength);
	String(text, length);


	return 0;
}

void cstring(char ctext[], int clength)		//cstring
{
	int c = 0;
	for (int i = 0; i < clength; i++)
	{
		if (ctext[i] != ctext[clength - i - 1])
		{
			c = 1;
			break;
		}
	}

	if (c == 1)
	{
		cout << ctext << " is not a palindrome (from cstring).\n";
	}
	else
	{
		cout << ctext << " is a palindrome (from cstring).\n";
	}
}

void String(string text, int length)	//String 
{
	int c = 0;
	for (int i = 0; i < length; i++)
	{
		if (text[i] != text[length - i - 1])
		{
			c = 1;
			break;
		}
	}

	if (c == 1)
	{
		cout << text << " is not a palindrome (from string).\n";
	}
	else
	{
		cout << text << " is a palindrome (from string).\n";
	}
}